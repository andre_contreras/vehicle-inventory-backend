# Quick start
## Requirements
- PHP 7.2+
- Composer

## Instructions
- `docker-compose up`
- `php bin/console doctrine:schema:create`
- `php bin/console doctrine:fixtures:load`
- `php bin/console server:start`

The latter will give you a port to specify in the Frontend
