<?php

namespace App\Entity;

use App\Repository\VehicleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=VehicleRepository::class)
 */
class Vehicle implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $model;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\Column(type="integer")
     */
    private $horse_power;

    /**
     * @ORM\ManyToMany(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinTable(name="vehicles_images",
     *     joinColumns={@ORM\JoinColumn(name="vehicle_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")})
     */
    private $images;

    /**
     * @ORM\ManyToOne(targetEntity="VehicleType", inversedBy="vehicles")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getHorsePower(): ?int
    {
        return $this->horse_power;
    }

    public function setHorsePower(int $hp): self
    {
        $this->horse_power = $hp;

        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return VehicleType
     */
    public function getType(): VehicleType
    {
        return $this->type;
    }

    /**
     * @param VehicleType $type
     * @return Vehicle
     */
    public function setType(VehicleType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return Vehicle
     */
    public function setCreatedBy(User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getImages(): PersistentCollection
    {
        return $this->images;
    }

    /**
     * @param Image $image
     * @return Vehicle
     */
    public function addImage(Image $image): self
    {
        if (!isset($this->images)) {
            $this->images = new ArrayCollection();
        }
        $this->images->add($image);

        return $this;
    }

    public function jsonSerialize(): array
    {
        $images = [];

        if ($this->images) {
            foreach ($this->images as $image) {
                $path = explode("/public", $image->getPath());

                $images[] = [
                    'url' => $path[1],
                ];
            }
        }

        return [
            'id' => $this->id,
            'model' => $this->model,
            'year'=> $this->year,
            'horse_power' => $this->horse_power,
            'type' => $this->type,
            'images' => $images,
            'created' => $this->created,
        ];
    }
}
