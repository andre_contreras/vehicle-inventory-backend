<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Vehicle;
use App\Entity\VehicleType;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1/vehicle", name="vehicle")
 */
class VehicleController extends AbstractController
{
    /**
     * @Route("", name="getAllVehicles", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $host = $request->getSchemeAndHttpHost();
        $entityManager = $this->getDoctrine()->getManager();

        $vehiclesRepository = $entityManager->getRepository(Vehicle::class);

        $vehiclesCount = $vehiclesRepository->count([]);
        $vehicles = $vehiclesRepository->findBy([], ['created' => 'asc'], 10);

        foreach ($vehicles as $vehicle) {
            $images = $vehicle->getImages();
            if ($images) {
                foreach ($images as $image) {
                    $image->publicUrl = $host.$image->getPath();
                }
            }
        }

        if ($vehicles) {
            return $this->json([
                'vehicles' => $vehicles,
                'meta' => [
                    'count' => $vehiclesCount,
                ]
            ]);
        } else {
            throw new HttpException(500, 'Server cannot fulfill the request');
        }
    }

    /**
     * @Route("", name="createVehicle", methods={"POST"})
     */
    public function create(Request $request, UploadableManager $uploadableManager): Response {
        $files = $request->files->get('images');
        $data = [
            'model' => $request->request->get('model'),
            'category' => $request->request->get('category'),
            'year' => $request->request->get('year'),
            'horse_power' => $request->request->get('horse_power'),
            'images' => $files,
        ];
        $entityManager = $this->getDoctrine()->getManager();

        $vehicleTypeRepository = $entityManager->getRepository(VehicleType::class);

        $vehicleType = $vehicleTypeRepository->find($data['category']);


        if ($vehicleType) {
            $newVehicle = new Vehicle();

            $newVehicle->setType($vehicleType);
            $newVehicle->setCreatedBy($this->getUser());
            $newVehicle->setYear($data['year']);
            $newVehicle->setHorsePower($data['horse_power']);
            $newVehicle->setModel($data['model']);

            if (isset($data['images'])) {
                foreach ($data['images'] as $image) {
                    $newImage = new Image();
                    $newImage->file = new UploadedFile($image, $image);

                    $uploadableManager->markEntityToUpload($newImage, $image);
                    $newVehicle->addImage($newImage);

                    $entityManager->persist($newImage);
                }
            }

            $entityManager->persist($newVehicle);
            $entityManager->flush();

            return $this->json([
                'message' => 'Vehicle created successfully!'
            ]);
        }

        throw new HttpException(400, 'Bad request');
    }
}
