<?php

namespace App\Controller;

use App\Entity\VehicleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1/vehicle-type", name="cars")
 */
class VehicleTypeController extends AbstractController
{
    /**
     * @Route("/", name="getAllVehicleTypes")
     */
    public function index(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $vehiclesTypeRepository = $entityManager->getRepository(VehicleType::class);

        $vehicleTypes = $vehiclesTypeRepository->findBy([], ['id' => 'asc'], 10);

        if ($vehicleTypes) {
            return $this->json([
                'vehicles_types' => $vehicleTypes
            ]);
        } else {
            throw new HttpException(500, 'Server cannot fulfill the request');
        }
    }
}
