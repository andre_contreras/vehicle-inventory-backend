<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
     {
         $this->passwordHasher = $passwordHasher;
     }
    public function load(ObjectManager $manager)
    {
        $users = [
            ['admin', ['admin'], 'superSecurePasswordForAdmin']
        ];

        foreach ($users as $user) {
            $newUser = new User($user[0], $user[1]);

            $newUser->setPassword($this->passwordHasher->hashPassword($newUser, $user[2]));

            $manager->persist($newUser);
        }

        $manager->flush();
    }
}
