<?php

namespace App\DataFixtures;

use App\Entity\Image;
use App\Entity\User;
use App\Entity\Vehicle;
use App\Entity\VehicleType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class VehicleFixtures extends Fixture
{
    private $container;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userRepo = $manager->getRepository(User::class);
        $uploadableManager = $this->container->get('stof_doctrine_extensions.uploadable.manager');

        $admin = $userRepo->findOneBy(['username' => 'admin']);

        $vehicleTypes = [
            [
                'name' => 'sedan',
                'wheel_number' => 4,
            ],
            [
                'name' => 'motorcycle',
                'wheel_number' => 2,
            ]
        ];

        foreach ($vehicleTypes as $vehicleKey => $vehicleType) {
            $newVehicleType = new VehicleType();

            $newVehicleType->setName($vehicleType['name']);
            $newVehicleType->setWheelNumber($vehicleType['wheel_number']);

            $manager->persist($newVehicleType);

            $vehicleTypes[$vehicleKey]['entity'] = $newVehicleType;
        }

        $faker = \Faker\Factory::create();

        $vehicles = [
            [
                'model' => 'Honda Civic',
                'year' => 2021,
                'horse_power' => 180,
                'type' => $vehicleTypes[0]['entity'],
                'created_by' => $admin,
                'images' => [
                    $faker->image(sys_get_temp_dir(), 500, 500, 'car'),
                    $faker->image(sys_get_temp_dir(), 500, 500, 'car'),
                ]
            ],
            [
                'model' => 'BMW i4',
                'year' => 2020,
                'horse_power' => 320,
                'type' => $vehicleTypes[0]['entity'],
                'created_by' => $admin,
                'images' => [
                    $faker->image(sys_get_temp_dir(), 500, 500, 'car'),
                    $faker->image(sys_get_temp_dir(), 500, 500, 'car'),
                ]
            ],
            [
                'model' => 'Harley-davidson Freewheeler',
                'year' => 2019,
                'horse_power' => 89,
                'type' => $vehicleTypes[1]['entity'],
                'created_by' => $admin,
                'images' => [
                    $faker->image(sys_get_temp_dir(), 500, 500, 'motorcycle'),
                    $faker->image(sys_get_temp_dir(), 500, 500, 'motorcycle'),
                ]
            ],
            [
                'model' => 'Yamaha Bolt',
                'year' => 2018,
                'horse_power' => 53,
                'type' => $vehicleTypes[1]['entity'],
                'created_by' => $admin,
                'images' => [
                    $faker->image(sys_get_temp_dir(), 500, 500, 'motorcycle'),
                    $faker->image(sys_get_temp_dir(), 500, 500, 'motorcycle'),
                ]
            ],
        ];

        foreach ($vehicles as $vehicle) {
            $newVehicle = new Vehicle();

            $newVehicle->setModel($vehicle['model']);
            $newVehicle->setHorsePower($vehicle['horse_power']);
            $newVehicle->setYear($vehicle['year']);
            $newVehicle->setType($vehicle['type']);
            $newVehicle->setCreatedBy($vehicle['created_by']);

            if ($vehicle['images']) {
                foreach ($vehicle['images'] as $image) {
                    $newImage = new Image();

                    $newImage->file = new UploadedFile($image, $image);

                    $uploadableManager->markEntityToUpload($newImage, $newImage->file);

                    $newVehicle->addImage($newImage);
                }
            }

            $manager->persist($newVehicle);
        }

        $manager->flush();
    }
}
